# Vert.x 
## Usage
```bash
mvn package
mvn exec:java

```
testing main route with curl:
```bash
curl -v http://localhost:8888\?name\="Francesco" 
# response: {"name":"Francesco","address":"0:0:0:0:0:0:0:1:57183","message":"Hello Francesco connected from 0:0:0:0:0:0:0:1:57183"}%
```

